package oop;

public class Person {

    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }



    void greeting() {
        System.out.println("Hello, I'm " + name);
    }



    @Override
    public String toString() {                  //ПКМ-> GENERATE -> toString();
        return "Person {" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
